<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the SavaPage project <https://savapage.org>
Copyright (c) 2020 Datraverse B.V. | Author: Rijk Ravestein 

SPDX-FileCopyrightText: © 2020 Datraverse BV <info@datraverse.com>
SPDX-License-Identifier: AGPL-3.0-or-later

SavaPage is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Translator: Rijk Ravestein <rijkr@savapage.org>
-->
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>

    <entry key="_translator_name">Rijk Ravestein</entry>
    <entry key="_translator_email">rijkr@savapage.org</entry>

    <!-- | ...............................................................| -->
    <entry key="title">Optionen</entry>

    <entry key="circuit-damaged">Beschädigt</entry>
    <entry key="circuit-open">Verbindung unterbrochen</entry>
    <entry key="blocked">Gesperrt</entry>

    <entry key="cat-user-source">Benutzerquelle</entry>
    <entry key="cat-user-creation">Benutzererstellung</entry>
    <entry key="cat-user-authentication">Benutzerauthentifizierung</entry>

    <entry key="cat-mail">Mail</entry>
    <entry key="cat-mail-messages">Nachrichten</entry>

    <entry key="prompt-mail-test">Eine Testnachricht senden. Drücken Sie die
        Test-Taste, nachdem Sie die Änderungen übernommen haben.</entry>
    <entry key="mail-test-to">An</entry>
    <entry key="button-mail-test">Testen</entry>

    <entry key="cat-backups">Sicherungskopien</entry>
    <entry key="cat-backup-location">Sicherungsort</entry>
    <entry key="button-backup-now">Jetzt sichern</entry>
    <entry key="backup-last-run">Letzten Lauf</entry>
    <entry key="backup-next-automatic-run">Nächste Sicherung</entry>

    <entry key="cat-backup-automatic">Automatische Sicherungskopien</entry>
    <entry key="backup-automatic-descript">Automatische Sicherungskopien erstellen einen
        periodischen Überblick über die Systemdatenbank</entry>
    <entry key="backup-automatic-tooltip">Der Sicherungszeitraum ist wöchentlich, aber jeder
        Zeitraum kann, wie im User Manual beschrieben, konfiguriert
        werden.</entry>

    <entry key="enable-automatic-backup">Automatische Sicherungskopien aktivieren</entry>

    <entry key="backups-keep-days-prompt">Sicherungskopien behalten für</entry>
    <entry key="backups-keep-days">Tage</entry>

    <entry key="backup-automatic-old-data-descript">Alte Daten löschen nach dem automatischen
        Sicherungskopie.</entry>

    <entry key="backup-delete-account-trxs">Kontotransaktionen löschen älter als</entry>
    <entry key="backup-delete-account-trx-days">Tage</entry>

    <entry key="backup-delete-doc-logs">Dokumente löschen älter als</entry>
    <entry key="backup-delete-doc-log-days">Tage</entry>

    <entry key="backup-delete-app-logs">Anwendungslogs löschen älter als</entry>
    <entry key="backup-delete-app-log-days">Tage</entry>

    <entry key="days">Tage</entry>

    <entry key="cat-advanced">Erweitert</entry>

    <entry key="admin-password-reset">Reset internal admin password</entry>
    <entry key="password-new">Neues Passwort</entry>
    <entry key="password-confirm">Passwort Bestätigen</entry>

    <entry key="jmx-password-reset">Passwort "{0}" zurücksetzen.</entry>
    <entry key="jmx-comment">Die Java VM überwachen mit einem Werkzeug, das auf
        die JMX-Spezifikation entspricht, wie Java VisualVM. Eine
        Verbindung mit Remote-Prozess:</entry>

    <entry key="button-apply">Anwenden</entry>
    <entry key="button-cancel">Annullieren</entry>
    <entry key="button-change">Ändern</entry>
    <entry key="button-change-group">Gruppe ändern</entry>
    <entry key="button-clear">Löschen</entry>
    <entry key="button-download">Herunterladen</entry>
    <entry key="button-sync-now">Jetzt synchronisieren</entry>
    <entry key="button-sync-test">Testen</entry>

    <entry key="auth-method-none">-</entry>
    <entry key="auth-method-unix">Unix</entry>
    <entry key="auth-method-ldap">LDAP</entry>
    <entry key="auth-method-custom">Anpassung</entry>

    <entry key="ldap-schema-type-open">OpenLDAP</entry>
    <entry key="ldap-schema-type-free">FreeIPA</entry>
    <entry key="ldap-schema-type-apple">Apple Open Directory</entry>
    <entry key="ldap-schema-type-activ">Microsoft Active Directory</entry>
    <entry key="ldap-schema-type-edir">Novell eDirectory</entry>
    <entry key="ldap-schema-type-google">Google Cloud Directory</entry>

    <entry key="ldap-host">Host</entry>
    <entry key="ldap-port">Port</entry>
    <entry key="ldap-basedn">Basis DN</entry>
    <entry key="ldap-use-ssl">SSL verwenden</entry>
    <entry key="ldap-use-ssl-trust-self-signed">Vertraue selbstsigniertes Zertifikat</entry>
    <entry key="ldap-admin-dn">Admin DN</entry>
    <entry key="ldap-admin-pw">Admin Passwort</entry>

    <entry key="ldap-user-fields">LDAP-Felder für alternative Authentifizierungs</entry>
    <entry key="ldap-user-id-number-field">ID Nummer</entry>
    <entry key="ldap-user-card-number-field">Kartennummer</entry>

    <entry key="ldap-card-format">Kartenformat</entry>
    <entry key="ldap-card-format-hex">HEX</entry>
    <entry key="ldap-card-format-dec">DEC</entry>
    <entry key="ldap-card-format-lsb">LSB</entry>
    <entry key="ldap-card-format-msb">MSB</entry>

    <entry key="import-users-from">Benutzern importieren aus einer Gruppe.</entry>
    <entry key="user-source-group-select">Wählen Sie eine Gruppe</entry>

    <entry key="all-users">[All users]</entry>

    <entry key="sync-header">Synchronisation</entry>
    <entry key="sync-schedule">Neue Benutzer importieren über Nacht</entry>
    <entry key="sync-user-update">Benutzerdaten aktualisieren</entry>
    <entry key="sync-delete-users">Benutzer löschen, die in der ausgewählten Quelle
        nicht vorhanden sind.</entry>
    <entry key="sync-messages">Nachrichten</entry>

    <entry key="user-auth-mode-name-pw">Benutzername</entry>
    <entry key="user-auth-mode-name-pw-dialog">Im Dialog anzeigen</entry>

    <entry key="user-auth-mode-email-pw">Email</entry>
    <entry key="user-auth-mode-email-pw-dialog">Im Dialog anzeigen</entry>

    <entry key="user-auth-mode-id-number">ID Nummer</entry>
    <entry key="user-auth-mode-id-number-pin">Erfordern PIN</entry>
    <entry key="user-auth-mode-id-number-mask">Eingabe maskieren</entry>
    <entry key="user-auth-mode-id-number-dialog">Im Dialog anzeigen</entry>

    <entry key="user-auth-mode-yubikey">YubiKey</entry>
    <entry key="user-auth-mode-yubikey-dialog">Im Dialog anzeigen</entry>

    <entry key="user-auth-mode-card-local">Lokale NFC-Karte</entry>
    <entry key="user-auth-mode-card-local-dialog">Im Dialog anzeigen</entry>

    <entry key="authentication-persistence">Persistenz</entry>
    <entry key="browser-local-storage">Browser Lokaler Speicher</entry>

    <entry key="users-pin">PIN</entry>
    <entry key="users-can-change-pin">Benutzer können ihre PIN ändern</entry>

    <entry key="prompt-trust">Vertrauen</entry>
    <entry key="webapp-user-auth-trust-cliapp-auth">User Client Vertrauen</entry>

    <entry key="user-auth-mode-card">NFC-Karte</entry>
    <entry key="user-auth-mode-card-pin">Erfordern PIN</entry>
    <entry key="user-auth-mode-card-self-assoc">Selbstverbindung</entry>

    <entry key="card-format">Format</entry>
    <entry key="local-card-format-hex">HEX</entry>
    <entry key="local-card-format-dec">DEC</entry>
    <entry key="local-card-format-lsb">LSB</entry>
    <entry key="local-card-format-msb">MSB</entry>

    <entry key="user-auth-mode-login">Anmeldemethoden</entry>
    <entry key="user-auth-mode-default">Standard Anmeldung</entry>
    <entry key="user-auth-mode-default-user">Benutzername</entry>
    <entry key="user-auth-mode-default-email">Email</entry>
    <entry key="user-auth-mode-default-number">ID Nummer</entry>
    <entry key="user-auth-mode-default-card">Lokale NFC-Karte</entry>
    <entry key="user-auth-mode-default-yubikey">YubiKey</entry>

    <entry key="smtp-host">Host</entry>
    <entry key="smtp-port">Port</entry>
    <entry key="smtp-username">Benutzername</entry>
    <entry key="smtp-password">Passwort</entry>

    <entry key="smtp-security-none">-</entry>
    <entry key="smtp-security-starttls">STARTTLS</entry>
    <entry key="smtp-security-ssl-tls">SSL/TLS</entry>

    <entry key="mail-from-address">Absenderadresse</entry>
    <entry key="mail-from-name">Absender</entry>
    <entry key="mail-reply-address">Antwortadresse</entry>
    <entry key="mail-reply-name">Antwort Name</entry>

    <entry key="on-demand-user-creation">Benutzererstellung auf Nachfrage</entry>
    <entry key="on-demand-login">Bei der ersten Anmeldung</entry>
    <entry key="on-demand-print">Auf den ersten Druck</entry>

    <entry key="pagometer-reset">Pagometers Löschen</entry>
    <entry key="pagometer-reset-descript">Mit dieser Aktion wird der Zähler der produzierten
        Seiten auf Null gesetzt. Bitte wählen Sie einen oder mehrere
        Pagometers zu setzen.</entry>
    <entry key="pagometer-reset-dashboard">Armaturenbrett</entry>
    <entry key="pagometer-reset-queues">Wartelisten</entry>
    <entry key="pagometer-reset-printers">Proxy Drucker</entry>
    <entry key="pagometer-reset-users">Benutzer</entry>

    <entry key="locale-header">Locale</entry>
    <entry key="locale-description">Leer lassen, um die System-Standard "{0}" zu
        akzeptieren.</entry>

    <entry key="papersize-header">Standard-Papierformat</entry>
    <entry key="papersize-description">Wählen Sie "Standard", um die System-Standard "{0}"
        zu akzeptieren.
    </entry>

    <entry key="default-papersize-system">Standard</entry>
    <entry key="default-papersize-letter">Letter</entry>
    <entry key="default-papersize-a4">A4</entry>

    <entry key="print-in-header">SafePages</entry>
    <entry key="print-in-encrypted-pdf-descript">Diese Option ermöglicht es verschlüsselte
        PDF-Dokumente zu drucken nach SafePages für Proxy Drucken.
        Exportieren von diese SafePages als PDF ist nicht erlaubt.
    </entry>
    <entry key="print-in-allow-encrypted-pdf">Verschlüsselte PDF für Proxy Drucken zulassen</entry>
    <entry key="print-in-clear-descript">Diese Optionen bestimmen den Lebenszyklus des
        Eingangsdokumenten.</entry>
    <entry key="print-in-clear-at-logout">Dokumente löschen im Web App-Logout</entry>
    <entry key="print-in-expiry-descript">Dokument Ablaufzeit (Eingabe Null zu deaktivieren)</entry>
    <entry key="print-in-expiry-signal-descript">Ablaufzeitsignal (Eingabe Null zu deaktivieren)</entry>
    <entry key="print-in-expiry-mins">Minuten</entry>
    <entry key="print-in-expiry-signal-mins">Minuten</entry>

    <entry key="converters-header">Konverter</entry>
    <entry key="libreoffice-enable">LibreOffice Konverter aktivieren</entry>
    <entry key="libreoffice-soffice-enable">Mehrere Dienste aktivieren</entry>

    <entry key="xpstopdf-enable">XPS zu PDF Konverter aktivieren</entry>
    <entry key="installed">installiert</entry>
    <entry key="not-installed">nicht installiert</entry>

    <entry key="button-config-editor">Konfigurationseditor (fortgeschritten)</entry>

    <entry key="cat-integration">Integration</entry>
    <entry key="papercut-button-test">Test</entry>
    <entry key="papercut-header-text">PaperCut Integration</entry>
    <entry key="papercut-enable">Aktivieren</entry>
    <entry key="papercut-server-text">PaperCut Server</entry>
    <entry key="papercut-host">Host</entry>
    <entry key="papercut-port">Port</entry>
    <entry key="papercut-token">Token</entry>

    <entry key="papercut-db-enable">Database</entry>
    <entry key="papercut-db-text">PaperCut Database</entry>
    <entry key="papercut-db-driver">Driver</entry>
    <entry key="papercut-db-url">URL</entry>
    <entry key="papercut-db-user">Benutzer</entry>
    <entry key="papercut-db-password">Passwort</entry>

    <entry key="prompt-period-from">Von</entry>
    <entry key="prompt-period-to">Bis</entry>

    <entry key="cat-mail-print">Mail Druck</entry>
    <entry key="imap-enable">Benutzern erlauben, Dokumente zu versenden</entry>
    <entry key="imap-host">Host</entry>
    <entry key="imap-port">Port</entry>
    <entry key="imap-username">Benutzername</entry>
    <entry key="imap-password">Passwort</entry>
    <entry key="imap-security-none">-</entry>
    <entry key="imap-security-starttls">STARTTLS</entry>
    <entry key="imap-security-ssl-tls">SSL/TLS</entry>
    <entry key="imap-folder-inbox">Inbox</entry>
    <entry key="imap-folder-trash">Trash</entry>
    <entry key="imap-button-test">Testen</entry>
    <entry key="imap-button-stop">Stoppen</entry>
    <entry key="imap-button-start">Starten</entry>
    <entry key="imap-max-file-mb">Maximale Anlagengröße (MB)</entry>
    <entry key="imap-max-files">Maximale Anhänge</entry>

    <entry key="cat-web-print">Web Druck</entry>
    <entry key="webprint-enable">Benutzern erlauben, Dokumente hochzuladen</entry>
    <entry key="webprint-enable-dropzone">Drag &amp; Drop aktivieren</entry>
    <entry key="webprint-max-file-mb">Maximale Dokumentgröße (MB)</entry>
    <entry key="webprint-ip-allowed">IP-Adressen erlaubt</entry>

    <entry key="cat-eco-print">Eco Druck</entry>
    <entry key="ecoprint-enable">Benutzern Eco Druck erlauben</entry>
    <entry key="ecoprint-discount-perc">Proxy Druck Rabatt Prozent</entry>
    <entry key="ecoprint-auto-threshold-page-count">Maximale Dokumentgröße (Seiten) für die
        automatische Filterung</entry>
    <entry key="ecoprint-resolution-dpi">Auflösung DPI</entry>

    <entry key="cat-internet-print">Internet Druck</entry>
    <entry key="prompt-internetprint-base-uri">IPP protocol://authority des Internet-Druckergerät
        URI:</entry>

    <entry key="button-register">Registrieren</entry>
    <entry key="button-refresh">Erneuern</entry>
    <entry key="button-online">Online</entry>
    <entry key="button-offline">Offline</entry>
    <entry key="button-details">Details</entry>
    <entry key="button-hide">Ausblenden</entry>

    <entry key="cat-proxy-print">Proxy Druck</entry>
    <entry key="cat-proxy-print-mode-fast">Schnell Druckmodus</entry>
    <entry key="cat-proxy-print-mode-hold">Halt Druckmodus</entry>
    <entry key="cat-proxy-print-mode-direct">Direct Druckmodus</entry>

    <entry key="proxyprint-non-secure">Nicht sichere Proxy Druck erlauben</entry>
    <entry key="proxyprint-non-secure-printer-group">Nur in Druckergruppe (leer lassen um alle Drucker
        zu ermöglichen).</entry>

    <entry key="proxyprint-fast-expiry-mins-prompt">SafePages überspringen deren Zeitpunkt der
        Erstellung oder Erneuerung älter ist als:</entry>
    <entry key="proxyprint-fast-expiry-mins">Minuten</entry>
    <entry key="proxyprint-fast-inherit-printin-enable">Übernahme der IPP-Optionen aus dem Print-in-Dokument</entry>

    <entry key="proxyprint-hold-expiry-mins-prompt">Direkt angehaltenen Aufträgen überspringen, deren
        Zeitpunkt der Erstellung oder Erneuerung älter ist als</entry>

    <entry key="proxyprint-hold-expiry-mins">Minuten</entry>

    <entry key="proxyprint-direct-expiry-secs-prompt">Zeitlimit für NFC-Karte Bewegung</entry>
    <entry key="proxyprint-direct-expiry-secs">Sekunden</entry>

    <entry key="proxyprint-general-prompt">Allgemein</entry>
    <entry key="proxyprint-max-copies">Maximale Anzahl Kopien pro Auftrag</entry>
    <entry key="proxyprint-max-pages">Maximale Anzahl der Seiten pro Auftrag</entry>

    <entry key="proxyprint-clear-inbox">Seiten löschen nach dem Drucken</entry>
    <entry key="proxyprint-clear-inbox-scope">Wählen Sie den Bereich</entry>
    <entry key="proxyprint-clear-inbox-scope-show-user">Dem Benutzer anzeigen</entry>

    <entry key="proxyprint-delegate-enable">Aktivieren</entry>
    <entry key="proxyprint-delegate-papercut-enable">Delegierter Druck integrieren</entry>
    <entry key="proxyprint-personal-papercut-enable">Persönlicher Druck integrieren</entry>

    <entry key="proxyprint-delegate-papercut-invoicing-header">Delegierer Abrechnung</entry>
    <entry key="proxyprint-delegate-papercut-invoicing-legend">Ein CSV-Export von PaperCut von Druckkosten für
        Delegierer
        innerhalb eines Zeitraums.</entry>
    <entry
        key="proxyprint-delegate-papercut-invoicing-accounts-prompt">Eine durch Leerzeichen getrennte Liste von Konten
        (leer
        lassen, um alle auszuwählen)</entry>

    <entry key="internal-users-enable">Interne Benutzer</entry>
    <entry key="internal-users-change-pw">Interne Benutzer können ihr Passwort ändern</entry>

    <!-- Financial -->
    <entry key="cat-financial">Finanz</entry>
    <entry key="cat-financial-general">Allgemein</entry>
    <entry key="financial-global-credit-limit">Standardkreditlimit</entry>
    <entry key="financial-printer-cost-decimals">Drucker-Kosten Dezimalzahlen</entry>
    <entry key="financial-decimals-other">Dezimalzahlen verwendet anderswo</entry>

    <entry key="financial-currency-code">Währungscode</entry>
    <entry key="financial-currency-code-help">Verwenden Sie den Server command line interface um
        die Währung zu ändern.</entry>

    <entry key="financial-payment-methods">Eine durch Kommata getrennte Liste von
        Zahlungsmethoden (leer lassen, wenn nicht erforderlich).</entry>
    <entry key="cat-financial-pos">Kasse</entry>
    <entry key="financial-pos-receipt-header">Kassenbon Kopftext</entry>

    <entry key="cat-financial-vouchers">Gutscheine</entry>
    <entry key="financial-user-vouchers-enable">Benutzern erlauben, Gutscheine einlösen</entry>
    <entry key="financial-voucher-card-header">Kopfzeile</entry>
    <entry key="financial-voucher-card-footer">Fußzeile</entry>
    <entry key="financial-voucher-card-fontfamily">Schriftart</entry>

    <entry key="cat-financial-user-transfers">Überweisungen</entry>
    <entry key="financial-user-transfer-enable">Benutzern erlauben, Gelder an andere Benutzer zu
        übertragen</entry>
    <entry key="financial-user-transfer-enable-search">Benutzern erlauben, nach andere Benutzern zu suchen</entry>
    <entry key="financial-user-transfer-enable-comments">Benutzern erlauben, um Kommentare hinzufügen zu
        manuellen Transfers</entry>

    <!-- -->
    <entry key="report-fontfamily-header">Berichte Schriftart</entry>
    <entry key="report-fontfamily-legend">Wählen Sie die Standardschriftart für PDF-Berichte.</entry>

    <!-- User Client -->
    <entry key="cliapp-auth-header">User Client Authentifizierung</entry>
    <entry key="cliapp-auth-trust-user-account">Systembenutzer vertrauen</entry>
    <entry key="cliapp-auth-trust-webapp-user-auth">User Web App vertrauen</entry>
    <entry key="cliapp-auth-admin-passkey">Admin passkey (leer lassen zu deaktivieren)</entry>

    <entry key="username-aliases-header">Benutzernamen Aliase</entry>

</properties>
